import { useEffect, useState } from 'react';

function App() {
	const [categories, setCategories] = useState(null);
	const [list, setList] = useState([]);

	useEffect(() => {
		fetchCategories();
	}, []);

	const fetchCategories = () => {
		fetch('https://api.publicapis.org/categories')
			.then((res) => res.json())
			.then((data) => {
				setCategories(data);
				setList(data);
			});
	};

	const filterList = ({ target }) => {
		const search = target.value.toLowerCase();

		setList(
			categories.filter((cate) => {
				return cate.toLowerCase().includes(search);
			})
		);
	};

	const renderList = () =>
		list.map((cate, i) => (
			<tr key={i}>
				<th scope="row">{i + 1}</th>
				<td>{cate}</td>
			</tr>
		));

	return (
		<div>
			<input
				type="text"
				onChange={filterList}
				placeholder="filter here"
			/>
			<div className="table-responsive">
				<table className="table">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">categories</th>
						</tr>
					</thead>
					<tbody>{renderList()}</tbody>
				</table>
			</div>
		</div>
	);
}

export default App;
